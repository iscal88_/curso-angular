app.controller("TodoListController", function($scope, $state) {
    var vm = this;

    vm.tareas = [{
        descripcion: 'Tarea número 1',
        seleccionado: false,
        fTerminado: null
    }, {
        descripcion: 'Tarea número 2',
        seleccionado: false,
        fTerminado: null
    }];
    vm.seleccionarTarea = seleccionarTarea;
    vm.borrarTarea = borrarTarea;
    vm.anadirTarea = anadirTarea;
    vm.nuevaTarea = null;
    vm.keyPress = keyPress;

    /////////////

    function seleccionarTarea(tarea) {
        tarea.seleccionado = !tarea.seleccionado;

        if (tarea.seleccionado) {
            tarea.fTerminado = new Date();
        }
    }

    function borrarTarea(tarea) {
        var indice = vm.tareas.indexOf(tarea);
        if (indice !== -1) {
            vm.tareas.splice(indice, 1);
        }
    }

    function anadirTarea() {
        vm.tareas.push({
           descripcion: vm.nuevaTarea,
            seleccionado: false,
            fTerminado: null
        });
        vm.nuevaTarea = null;
    }

    function keyPress(keyEvent) {
        if (keyEvent.which === 13) {
            anadirTarea();
        }
    }

});
