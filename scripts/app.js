
var app = angular
    .module('appBabel', ['ui.router'])
    //.config(configLocales)
    .config(configRoutes)
    .run(initialize);

/////////////////

function configLocales($translateProvider) {
    $translateProvider
        .useStaticFilesLoader({
            prefix: 'locales/',
            suffix: '.json'
        })
        .registerAvailableLanguageKeys(['pt', 'en'], {
            'pt': 'pt',
            'pt_PT': 'pt',
            'en': 'en',
            'en_GB': 'en',
            'en_US': 'en'
        })
        .preferredLanguage('pt')
        .fallbackLanguage('pt')
        .useSanitizeValueStrategy('escapeParameters');
}

function configRoutes($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise("/home");

	$stateProvider
		.state("app", {
			url: "",
			templateUrl: "views/layout-app.html",
			abstract: true
		})
		.state("app.home", {
			url: "/home",
			views: {
				'content': {
					controller: 'HomeController',
					controllerAs: 'vm',
					templateUrl: 'views/home.html'
				}
			}
		})
		.state("app.todo-list", {
            url: "/todo",
            views: {
                'content': {
                    controller: 'TodoListController',
                    controllerAs: 'vm',
                    templateUrl: 'views/todo-list.html'
                }
            }
		});
}

function initialize($rootScope) {
	$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
		console.log('$stateChangeError');
	});

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
		console.log('$stateChangeStart');
	});
}

