app.filter('FechaTarea', function ($filter) {
    return function (input) {
        var hora = input.getHours();
        var minutos = input.getMinutes();
        var segundos = input.getSeconds();

        function z(value) {
            if (value.toString().length === 1) {
                value = "0" + value;
            }
            return value;
        }

        return " > Terminada a las " + z(hora) + ":" + z(minutos) + ":" + z(segundos);
    };
});
