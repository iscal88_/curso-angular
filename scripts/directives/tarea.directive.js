app.directive("tarea", function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: "./views/tarea.html",
        scope: {
            info: '=info',
            onDelete: '=onDelete',
            onSelect: '=onSelect'
        },
        link: function(scope, el) {
        },
        controller: function($scope) {
            var vm = $scope;
            vm.borrarTarea = borrarTarea;
            vm.seleccionarTarea = seleccionarTarea;

            //////////////

            function borrarTarea(t){
                $scope.onDelete(t);
            }

            function seleccionarTarea(t) {
                $scope.onSelect(t);
            }
        }
    };
});
